# Administrador de usuarios

Este proyecto se realizó con la versión de angular/cli 11.2.9

# Descripción

Es una aplicación sencilla la cúal cuenta con las siguientes caracteristicas:
* Login
  > Para acceder a las secciones de la aplicación necesitas iniciar sesión con el correo: eve.holt@reqres.in
  > y cualquier contraseña, minimo 8 carácteres, minusculas, mayusculas y numeros.
* Home
  > En este sección se muestra el listado de los usuarios.
* Detalle
  > En esta sección muestra el detalle del usuario seleccionado y los posts que le pertenecen.
  > Podrás editar la información del usuario o eliminar alguno de sus posts.
* Edit
  > En esta sección podrás editar la información del usuario

Utiliza dos APIs
* https://reqres.in/ con el que se obtiene un token de inicio de sesión y podemos obtener una lista de usuarios.
* https://jsonplaceholder.typicode.com es una API que nos permitió obtener una lista de posts en base a un numero de usuario.

# Demo

El demo de la aplicación se encuentra en el siguiente link: https://gracious-meninsky-102eba.netlify.app/

# Probar en local

Qué se necesita para ejecutar la aplicación:
* tener la versión 11.2.9 de angular/cli
* clonar este repo https://gitlab.com/carlos.dominguez1/umvel.git
* y ejecutar **ng serve -o**

 para tener un login exitoso el correo es: eve.holt@reqres.in

