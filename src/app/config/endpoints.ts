export const apiEndpoints = {
    "login": "https://reqres.in/api/login",
    "users": "https://reqres.in/api/users",
    "posts": "https://jsonplaceholder.typicode.com/posts"
}