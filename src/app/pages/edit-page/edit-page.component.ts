import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserModel } from 'src/app/models/user.model';
import { UsersService } from 'src/app/services/users.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss']
})
export class EditPageComponent implements OnInit {
  public user: UserModel;
  public editForm: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private usersService: UsersService,
    private FormBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
  ) { }

  async ngOnInit() {
    try {
      const id = this.route.snapshot.params.id
      const userResponse = <any> await this.usersService.getById(id);
      this.user = userResponse.data;

      this.editForm = this.FormBuilder.group({
        first_name: [this.user.first_name, Validators.required],
        last_name: [this.user.last_name, Validators.required],
        email: [this.user.email, [Validators.required, Validators.email]]
      });
    } catch(e) {
      console.error('[ERROR] ', e.error)
    }
  }

  get form() { return this.editForm.controls; }
  get firstNameInput() { return this.editForm.controls.first_name; }
  get lastNameInput() { return this.editForm.controls.last_name; }
  get emailInput() { return this.editForm.controls.email; }

  async Save() {
    try {
      await this.usersService.editUser(this.user.id, this.editForm.value);
      this.user = {...this.user, ...this.editForm.value};
      this._snackBar.open('Update successfully', null, {duration: 2000});
    } catch(e) { console.error('[ERROR] ', e.error)}
  }
}
