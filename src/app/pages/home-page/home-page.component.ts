import { Component, OnInit } from '@angular/core';
import { UserResponseModel } from 'src/app/models/user.model';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  public usersData: UserResponseModel;

  constructor(private usersService: UsersService) { }

  async ngOnInit() {
    try {
      const users = <UserResponseModel> await this.usersService.getAll();
      this.usersData = users;
      console.log(users);
      
    } catch(e) {
      console.error('[ERROR] ', e.error)
    }
  }

  async PageEvent(paginator) {
    try {
      console.log(paginator);
      const users = <UserResponseModel> await this.usersService.getPage(paginator.pageIndex +1);
      this.usersData = users;
    } catch(e) {
      console.error('[ERROR] ', e.error)
    }
    
  }
}
