import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  private patters = {
    email: /^([a-zA-Z]|[0-9]+[a-zA-Z]+)([a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~])*@[a-zA-Z0-9\-]+\.[a-zA-Z]{2,}/,
    password: /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&#.])[A-Za-z0-9$@$!%*?&#.].{7,19}$/
  }
  public loginForm: FormGroup;
  public errorMessage: string;
  public errorLogin: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
  ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.patters.email)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.pattern(this.patters.password)]],
    });
  }

  get form() { return this.loginForm.controls; }
  get emailInput() { return this.loginForm.controls.email; }
  get passwordInput() { return this.loginForm.controls.password; }

  async Submit() {
    this.errorLogin = false;
    console.log('Submitting...', this.loginForm.value);
    try {
      await this.loginService.Login(this.loginForm.value);
      console.log('Loggin success');
      
    } catch(e) {
      console.error('[ERROR] ', e.error)
      this.errorLogin = true;
      this.errorMessage = e.error.error;
    }
  }
}
