import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModel, UserResponseModel } from 'src/app/models/user.model';
import { UsersService } from 'src/app/services/users.service';
import { PostsService } from 'src/app/services/posts.service';
import { PostCommentsModel, PostModel } from 'src/app/models/post.model';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit {
  public user: UserModel;
  public posts: PostModel[] = [];
  constructor(
    private route: ActivatedRoute,
    private usersService: UsersService,
    private postService: PostsService,
    private dialog: MatDialog,
  ) { }

  async ngOnInit() {
    try {
      const id = this.route.snapshot.params.id
      const userResponse = <any> await this.usersService.getById(id);
      this.user = userResponse.data;
      this.posts = <PostModel[]> await this.postService.getPostByUserId(id);
      for (let i = 0; i< this.posts.length; i++) {
        this.posts[i].comments = <PostCommentsModel[]> await this.postService.getCommnents(this.posts[i].id);
      }
    } catch(e) {
      console.error('[ERROR] ', e.error)
    }
    
  }

  DeletePost(id) {
    const dialogRef = this.dialog.open(DialogComponent);
    dialogRef.afterClosed().subscribe( async (del) => {
      if (del) {
        try {
          await this.postService.deletePost(id);
          this.posts = this.posts.filter( (post:PostModel) => post.id !== id);
        } catch(e) { console.error('[ERROR] ', e.error)}
      }
    });
  }

}
