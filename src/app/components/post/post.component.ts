import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PostModel } from 'src/app/models/post.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input() post: PostModel;
  @Output() deletePost = new EventEmitter;
  constructor() { }

  ngOnInit(): void {
  }

  Delete(id) {
    this.deletePost.emit(id);
  }

}
