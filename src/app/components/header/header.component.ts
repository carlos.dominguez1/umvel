import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title: string;
  @Input() back: boolean = false;
  constructor(
    private location: Location,
    public auth: LoginService,
  ) { }

  ngOnInit(): void {
  }

  Back() {
    this.location.back();
  }
}
