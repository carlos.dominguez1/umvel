import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginModel } from '../models/login.model';
import { apiEndpoints } from './../config/endpoints';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router) { }

  async Login(User: LoginModel) {
    const loginRes = <LoginModel>await this.http.post(apiEndpoints.login, User).toPromise();
    sessionStorage.setItem('token', loginRes.token);
    this.router.navigate(['']);
  }

  Logout() {
    sessionStorage.clear();
    this.router.navigate(['login']);
  }

  isAuth() {
    const token = sessionStorage.getItem('token') || '';
    return token !== '';
  }
}
