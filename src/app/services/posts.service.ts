import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { apiEndpoints } from '../config/endpoints';
import { PostCommentsModel } from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }

  getPostByUserId(id: number) {
    const params = new HttpParams().set('userid', id.toString());
    return this.http.get(apiEndpoints.posts, {params}).toPromise();
  }

  deletePost(idPost) {
    return this.http.delete(`${apiEndpoints.posts}/${idPost}`).toPromise();
  }

  getCommnents(postId) {
    return this.http.get(`${apiEndpoints.posts}/${postId}/comments`).toPromise();
  }
}
