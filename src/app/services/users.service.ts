import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { UserModel, UserResponseModel } from '../models/user.model';
import { apiEndpoints } from './../config/endpoints';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(apiEndpoints.users).toPromise();
  }
  
  getPage(page: number) {
    const params = new HttpParams().set('page', page.toString());
    return this.http.get(apiEndpoints.users, {params}).toPromise();
  }

  getById(id: number) {
    return this.http.get(`${apiEndpoints.users}/${id}`).toPromise();
  }

  editUser(userId: number, user: UserModel) {
    return this.http.put(`${apiEndpoints.users}/${userId}`, user).toPromise();
  }
}
