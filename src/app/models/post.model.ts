export interface PostModel {
    userId: number,
    id: number,
    title: string,
    body: string,
    comments?: PostCommentsModel[],
}

export interface PostCommentsModel {
    postId: number,
    id: number,
    name: string,
    email: string,
    body: string
}